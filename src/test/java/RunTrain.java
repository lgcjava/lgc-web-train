import com.mindao.utils.jetty.JettyUtils9;

public class RunTrain {
	public static void main(String[] args) throws Exception {
		System.setProperty("org.mortbay.util.URI.charset", "UTF-8");
		JettyUtils9.run("train", 80);
	}

}
