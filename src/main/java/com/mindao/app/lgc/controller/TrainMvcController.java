package com.mindao.app.lgc.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mindao.app.lgc.util.TrainUtils;

/**
/* 版权所有： 广州敏道科技有限公司
/*
/* 功能描述：
/*
/* 创 建 人：李国才
/* 创建时间：2016年7月7日 下午5:17:48 	
 **/
@Controller
@RequestMapping("/lgc/trainmvc")
public class TrainMvcController{
	@RequestMapping
	public String login(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model rhs){
		return "lgc/trainmvc/login";
		
	}
	
	@RequestMapping
	public String main(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model rhs){
		return "lgc/trainmvc/main";
	}
	
	
	@RequestMapping
	public String logout(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model rhs){
		session.invalidate();
		return "lgc/trainmvc/login";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String submitLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model rhs) throws IOException{
		 String account=request.getParameter("account");
		 String pwd=request.getParameter("pwd");
		 if ("zs".equals(account) && "123".equals(pwd)){
			 request.getSession().setAttribute("account", account);
			 //response.sendRedirect("hello");
			 response.sendRedirect(request.getContextPath()+"/lgc/trainmvc/main.do");
		 }else{
			 String errInfo="";
			 if ("zs".equals(account)){
				 if (!"123".equals(pwd)){	 
					 errInfo="wrongPassword";
				 }
			 }else{
				 errInfo="wrongName";
			 }
 
			 response.sendRedirect(request.getContextPath()+"/lgc/trainmvc/login.do?errInfo="+errInfo);
		 
		 }
		 
		return "lgc/trainmvc/login";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public void getTaskNumber(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub 
		String account=(String)request.getSession().getAttribute("account");
		Integer taskNum= TrainUtils.getTaskNumber(account);
		response.getWriter().write(taskNum.toString());
 
	}

}
