package com.mindao.app.lgc.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserCheckFilter implements Filter {
	private String userCheck;
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpRequest=(HttpServletRequest)request;
		if ("true".equals(userCheck)){
			HttpServletRequest req=(HttpServletRequest)request;
			String account=(String)req.getSession().getAttribute("account");
			if (account==null){
				HttpServletResponse resp=(HttpServletResponse)response;
				resp.sendRedirect(httpRequest.getContextPath()+ "/app/lgc/train/login.jsp?errInfo=notLogin");
			}else{ 
				chain.doFilter(request, response);//放行。让其走到下个链或目标资源中
			}
		}else{
			chain.doFilter(request, response);//放行。让其走到下个链或目标资源中
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		userCheck= filterConfig.getInitParameter("userCheck");
		System.out.println(userCheck);
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	
	

}
