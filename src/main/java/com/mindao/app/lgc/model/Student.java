package com.mindao.app.lgc.model;
/**
/* 版权所有： 广州敏道科技有限公司
/*
/* 功能描述：
/*
/* 创 建 人：李国才
/* 创建时间：2016年7月8日 上午6:30:35 	
 **/
public class Student {
	private String pwd;
	private String account;
 
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
}
