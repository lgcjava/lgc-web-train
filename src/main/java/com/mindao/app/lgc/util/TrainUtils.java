package com.mindao.app.lgc.util;

import java.util.HashMap;
import java.util.Map;

import com.mindao.app.lgc.model.Student;

/**
/* 版权所有： 广州敏道科技有限公司
/*
/* 功能描述：培训公共类
/*
/* 创 建 人：李国才
/* 创建时间：2016年7月6日 下午5:03:08 	
 **/
public class TrainUtils {
	static Map<String,Integer> taskNumberMap=new HashMap<>();
	
	public static Map<String,Student> studentMap=new HashMap<>();
	
	/**
	 * 得到待办条数，假设每次取一次增加一条待办
	 * @param account
	 * @return
	 */
	public static Integer getTaskNumber(String account){
		Integer num= taskNumberMap.get(account);
		if (num==null){
			num=0;
		}
		num++;
		taskNumberMap.put(account, num);
		return num;
	}
}
