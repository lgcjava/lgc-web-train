package com.mindao.app.lgc.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mindao.app.lgc.model.Student;
import com.mindao.app.lgc.util.TrainUtils;

/**
 *登陆servlet
 */
 
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * Default constructor. 
     */
    public LoginServlet() {
        // TODO Auto-generated constructor stub
    }
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String s=request.getParameter("s");
    	System.out.println(s);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse request)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 HttpServletRequest httpRequest=(HttpServletRequest)request;
		 
		// if (request.getParameter("errInfo")!=null)  response.getWriter().print(request.getParameter("errInfo") ; 
		 request.setCharacterEncoding("UTF-8"); //Post时，与页面前端charset一致即可。
		 String account=request.getParameter("account");
		 String pwd=request.getParameter("pwd");
		 
		 Student stu=TrainUtils.studentMap.get(account);
		 if (("zs".equals(account) && "123".equals(pwd) )||(stu!=null && stu.getPwd().equals(pwd)) ){
			 request.getSession().setAttribute("account", account);
			 //response.sendRedirect("hello");
			 response.sendRedirect(httpRequest.getContextPath()+"/app/lgc/train/main.jsp");
		 }else{
			 String errInfo="";
			 if ("zs".equals(account)){
				 if (!"123".equals(pwd)){	 
					 errInfo="wrongPassword";
				 }
			 }else{
				 errInfo="wrongName";
			 }

		      response.setContentType("text/html;charset=utf-8");
		      response. setCharacterEncoding("UTF-8");
			 response.sendRedirect(httpRequest.getContextPath()+"/app/lgc/train/login.jsp?errInfo="+errInfo);
			 //request.getRequestDispatcher("/login.html?errInfo="+errInfo).forward(request, response);
		 }
		 
	}


}
