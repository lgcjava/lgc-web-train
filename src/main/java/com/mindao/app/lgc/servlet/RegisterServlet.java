package com.mindao.app.lgc.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mindao.app.lgc.model.Student;
import com.mindao.app.lgc.util.TrainUtils;

/**
 *登陆servlet
 */
 
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * Default constructor. 
     */
    public RegisterServlet() {
        // TODO Auto-generated constructor stub
    }
 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse request)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 HttpServletRequest httpRequest=(HttpServletRequest)request;
		
		 request.setCharacterEncoding("UTF-8"); //Post时，与页面前端charset一致即可。
		 String account=request.getParameter("account");
		 String pwd=request.getParameter("pwd");
		 if (account!=null && account.length()>0 && pwd!=null){
			 Student stu=new Student();
			 stu.setAccount(account);
			 stu.setPwd(pwd);
			 TrainUtils.studentMap.put(account, stu);
		      response.setContentType("text/html;charset=utf-8");
		      response. setCharacterEncoding("UTF-8");
			 response.sendRedirect(httpRequest.getContextPath()+"/app/lgc/train/login.jsp?errInfo=regiseter Seccessful");
		 }else{
 
			 
			 response.sendRedirect(httpRequest.getContextPath()+"/app/lgc/train/regiter.jsp?errInfo=data Uncomplete!");
			 //request.getRequestDispatcher("/login.html?errInfo="+errInfo).forward(request, response);
		 }
		 
	}


}
