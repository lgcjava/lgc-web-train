package com.mindao.app.lgc.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mindao.app.lgc.util.TrainUtils;

/**
 * 读取待办条数Servlet
 */
 
public class TaskNumberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public TaskNumberServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub 

      
	  String account=(String)request.getSession().getAttribute("account");	
      response.setContentType("text/html;charset=utf-8");
      response. setCharacterEncoding("UTF-8");
      
      PrintWriter out = response.getWriter(); 
      String s= TrainUtils.getTaskNumber(account).toString();
      out.println(s); //利用PrintWriter对象的方法将数据发送给客户端 　　
      out.close();
	}



}
